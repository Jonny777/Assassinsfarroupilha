package revo.screens;

import revo.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author Jonny
 */
public class StartController implements Initializable {
    
    
    @FXML Label label;
    
    public void iniciar(){
        Main.trocaTela("screens/Menu.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    
}
