package Unidades;
public class LanceiroNegro extends Unidade {
    private int HP = 100, dano = 75;
    private String nome = "Lanceiro Negro";

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int getHP() {
        return HP;
    }
    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getDano() {
        return dano;
    }
    public void setDano(int dano) {
        this.dano = dano;
    }

}
