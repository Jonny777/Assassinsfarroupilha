package revo.screens;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import revo.Main;

public class CavaleiroController implements Initializable {

    public void menu(){
        Main.trocaTela("screens/Menu.fxml");
    }
    
    public void voltar(){
        Main.trocaTela("screens/Unidades.fxml");
    }
    
    public void addAtk(){
        
    }
    
    public void addHp(){
        
    }
    
    public void tirarAtk(){
        
    }
    
    public void tirarHp(){
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
