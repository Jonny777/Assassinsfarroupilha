package revo.screens;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import revo.Main;

public class MenuController implements Initializable {

    public void unidades(){
        Main.trocaTela("screens/Unidades.fxml");
    }
    
    public void jogar(){
        Main.trocaTela("screens/Criação.fxml");
    }
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
