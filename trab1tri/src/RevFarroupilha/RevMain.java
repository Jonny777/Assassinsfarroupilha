package RevFarroupilha;

import Unidades.Espadachim;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RevMain extends Application {
    
    private static Stage stage;

    public static Stage getStage() {
        return stage;
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        
        try{
            root = FXMLLoader.load(RevMain.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TelaInicial.fxml"));
        
        Scene scene = new Scene(root);
        //observar essa linha = 
        RevMain.stage = stage;
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

        Espadachim a = new Espadachim();
        a.u1Stats();
    }
    
}
