package revo.screens;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import revo.Main;

public class UnidadesController implements Initializable {

    public void atirador(){
        Main.trocaTela("screens/Atirador.fxml");
    }
    
    public void espadachim(){
        Main.trocaTela("screens/Espadachim.fxml");
    }
    
    public void cavaleiro(){
        Main.trocaTela("screens/Cavaleiro.fxml");
    }
    
    public void assassino(){
        Main.trocaTela("screens/Assassino.fxml");
    }
    
    public void menu(){
        Main.trocaTela("screens/Menu.fxml");
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
