package revo.screens;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import revo.Main;
import revo.units.Exercitos;

/**
 * FXML Controller class
 *
 * @author Jonny
 */

public class CriaçãoController implements Initializable {

    ArrayList<Object> player = new ArrayList();
    
    @FXML
    private Label assassino;
    
    @FXML
    private Label cavaleiro;
    
    @FXML
    private Label atirador;
    
    @FXML
    private Label espadachim;
    
    @FXML 
    private Label recursos;
    
    public void voltar(){
        Main.trocaTela("screens/Menu.fxml");
    }
    
    // Criação das Unidades:
    private int money = 4500;
    private int A1 = 0;
    public void createA(){ //Atirador
        if(money>=50) {
            Exercitos.createA();
            A1++;
            money -= 50;
            recursos.setText(String.valueOf(money));
            atirador.setText(String.valueOf(A1));
        }
    }
    
    private int B1 = 0;
    public void createB(){ //Espadachim
        if(money>=75) {
            Exercitos.createB();
            B1++;
            money -= 75;
            recursos.setText(String.valueOf(money));
            espadachim.setText(String.valueOf(B1));
        }
    }
    
    private int C1 = 0;
    public void createC(){ //Cavaleiro
        if(money>=125) {
            Exercitos.createC();
            C1++;
            money -= 125;
            recursos.setText(String.valueOf(money));
            cavaleiro.setText(String.valueOf(C1));
        }
    }
    
    private int D1 = 0;
    public void createD(){ //Assassino
        if(D1<3 && money>=250){
            Exercitos.createD();
            D1++;
            money -= 250;
            recursos.setText(String.valueOf(money));
            assassino.setText(String.valueOf(D1));
        }      
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
