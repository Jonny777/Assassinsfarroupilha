package revo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Jonny
 */
public class Main extends Application {
    
private static Stage stage;

    public static Stage getStage() {
        return stage;
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(Main.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("screens/Start.fxml"));
        
        Scene scene = new Scene(root);
        Main.stage = stage;
        
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(String[] args) {        
        launch(args);
    }
    
}
