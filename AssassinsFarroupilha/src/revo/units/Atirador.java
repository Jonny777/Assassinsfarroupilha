package revo.units;

import java.util.Random;

public final class Atirador extends Unit {
    protected int hp = 300;
    protected int dano = 150;
    private final String nome = "Atirador";
    
    public int atiradorAttack(){
        int crit;
        Random r = new Random();
        if(r.nextInt()%100<25){
            crit = dano*3;
            return crit;
        }
        else {
            return dano;
        }
    }
}
