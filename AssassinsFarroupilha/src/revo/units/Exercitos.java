package revo.units;

import java.util.ArrayList;

/**
 *
 * @author jonny
 */
public class Exercitos {
    
    private static ArrayList<Unit> easy = new ArrayList();
    private static ArrayList<Unit> medium = new ArrayList();
    private static ArrayList<Unit> hard = new ArrayList();
    private static ArrayList<Unit> player = new ArrayList();
    
    //Aliados
    public static void createA(){ //Atirador
        Atirador A = new Atirador();
        player.add(A);
    }
    
    public static void createB(){ //Espadachim
        Espadachim B = new Espadachim();
        player.add(B);
    }
    
    public static void createC(){ //Cavaleiro
        Cavaleiro C = new Cavaleiro();
        player.add(C);
    }
    
    public static void createD(){ //Assassino
        Assassino D = new Assassino();
        player.add(D);
    }
    
    //Inimigos
    public void easy(){
        
        for(int i = 0; i < 50; i ++) {
            Atirador A = new Atirador();
            easy.add(A);
        }
        for(int i = 0; i < 25; i ++) {
            Espadachim B = new Espadachim();
            easy.add(B);
        }
        for(int i = 0; i < 10; i ++) {
            Cavaleiro C = new Cavaleiro();
            easy.add(C);
        }
    }
    
    public void medium(){
        
        for(int i = 0; i < 100; i ++) {
            Atirador A = new Atirador();
            medium.add(A);
        }
        for(int i = 0; i < 50; i ++) {
            Espadachim B = new Espadachim();
            medium.add(B);
        }
        for(int i = 0; i < 20; i ++) {
            Cavaleiro C = new Cavaleiro();
            medium.add(C);
        }
    }
    
    public void hard(){
        
        for(int i = 0; i < 200; i ++) {
            Atirador A = new Atirador();
            hard.add(A);
        }
        for(int i = 0; i < 100; i ++) {
            Espadachim B = new Espadachim();
            hard.add(B);
        }
        for(int i = 0; i < 50; i ++) {
            Cavaleiro C = new Cavaleiro();
            hard.add(C);
        }
    }
    
    //getters e setters arrayslists
    public static ArrayList<Unit> getEasy() {
        return easy;
    }

    public static void setEasy(ArrayList<Unit> easy) {
        Exercitos.easy = easy;
    }

    public static ArrayList<Unit> getMedium() {
        return medium;
    }

    public static void setMedium(ArrayList<Unit> medium) {
        Exercitos.medium = medium;
    }

    public static ArrayList<Unit> getHard() {
        return hard;
    }

    public static void setHard(ArrayList<Unit> hard) {
        Exercitos.hard = hard;
    }

    public static ArrayList<Unit> getPlayer() {
        return player;
    }

    public static void setPlayer(ArrayList<Unit> player) {
        Exercitos.player = player;
    }
    
    
}
