package Unidades;
public class Espadachim extends Unidade{
    private int HP = 350, dano = 75;
    private String nome = "Espadachim";
    
    public void u1Stats(){
        System.out.println("NOME: " + this.getDano());
        System.out.println("HP: " + this.getHP());
        System.out.println("DANO: " + this.getDano());
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getHP() {
        return HP;
    }
    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getDano() {
        return dano;
    }
    public void setDano(int dano) {
        this.dano = dano;
    }
    
}
