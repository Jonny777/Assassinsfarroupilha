package revo.units;
/**
 *
 * @author jonny
 */
public abstract class Unit {
    
    private int hp, dano;
    private String nome;
    
    @Override
    public String toString() {
        return "Unit[" + "HP: " + hp + ", Dano: " + dano + ", Nome: " + nome + ']';
    }
    
    //Getters e Setters
    public int getHp() {
        return hp;
    }
    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getDano() {
        return dano;
    }
    public void setDano(int dano) {
        this.dano = dano;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
}
